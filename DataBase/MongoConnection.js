#!/usr/local/bin/node
'use strict';

const Mongoose = require('mongoose');
const config = require('../config');
    
var promise = Mongoose.connect(config.Database.uri, {
  useMongoClient: true,
});
  
Mongoose.Promise = global.Promise;
const db = Mongoose.connection;

db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function callback() {
    console.log("Connection with database succeeded.");
});
    
exports.Mongoose = Mongoose;
exports.db = db;
