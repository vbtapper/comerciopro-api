#!/usr/local/bin/node
'use strict';

const Mongoose              = require('mongoose');
const Schema                = Mongoose.Schema;
const bcrypt                = require('bcryptjs');

const ProductModel = new Schema({
    created: { type: Date, default: Date.now },
    productPublicID: {type: String, required: true},
    reordeble: {type: Boolean, required: true, default: false},
    daysPerReorder: {type: Number, required: true, default: 1},
    sellingFrequence: {type: Number, required: true, default: 1},
    inventoryMethod: {type: String, required: true, default: 'fifo'},
    teamID: {type: String, required: true},
    general: {
        productName: {type: String, required: true},
        metaTagTitle: {type: String, required: true},
        metaTagDescription: {type: String, required: true},
        metaTagKeywords: {type: String},
        description: {type: String, required: true},   
        vendor: {type: String, required: true},
    },
    data: {
        model: {type: String, required: true},
        category: {type: String, required: true},
        brand: {type: String, required: true},
        location: {type: String, required: true},
        quantity: {type: Number, required: true},
        status: {type: String, required: true},
    },
    images: [{
        name: {type: String, required: true},
        path: {type: String, required: true},
        position: {type: Number, required: true}
    }],
    costs: {
        costPerItem: {type: Number, required: true, default: 0},
        totalValue: {type: Number, required: true, default: 0},
        sellingPrice: {type: Number, required: true},
        calculatedProfitPerItem: {type: Number, required: true, default: 0}
    },
    attributes: [{
        id: {type: String},
        type: {type: String},
        value: {type: String},
        key: {type: String},
        quantity: {type: Number},
        additionalCost: {type: Number, default: 0},
        purchaseOption: {type: Boolean}
    }]
});

module.exports = {
    ProductModel: Mongoose.model('product', ProductModel)
};