#!/usr/local/bin/node
'use strict';

const Mongoose              = require('mongoose');
const Schema                = Mongoose.Schema;
const bcrypt                = require('bcryptjs');

const SALT_WORK_FACTOR = 10;

const ClientModel = new Schema({
    created: { type: Date, default: Date.now },
    email: { type: String, unique: true, required: true },
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    teamID: {type: String, required: true},
    password: { type: String, required: true },
    addresses: [{
        street: {type: String},
        city: {type: String},
        country: {type: String}
    }]
});

ClientModel.pre('save', function(next) {
    var client = this;
    // only hash the password if it has been modified (or is new)
    if (!client.isModified('password')) return next();
    bcrypt.hash(client.password, SALT_WORK_FACTOR, function(err, hash) {
   
        if (err) return next(err);
        // override the cleartext password with the hashed one
        client.password = hash;
        next();
    });
});

var client = Mongoose.model('client', ClientModel);
module.exports = {
    ClientModel: client
};