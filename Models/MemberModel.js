#!/usr/local/bin/node
'use strict';

const Mongoose              = require('mongoose');
const Schema                = Mongoose.Schema;
const bcrypt                = require('bcryptjs');

const SALT_WORK_FACTOR = 10;

const MemberModel = new Schema({
    created: { type: Date, default: Date.now },
    email: { type: String, unique: true, required: true },
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    scope: [{type: String}],
    teamID: {type: String, required: true},
    password: { type: String, required: true },
    active: {type: Boolean, default: true, required: true},
    sessiontoken: { type: String },
    firsttimelogin: {type: Boolean, default: true}
});

MemberModel.pre('save', function(next) {
    var customer = this;
    // only hash the password if it has been modified (or is new)
    if (!customer.isModified('password')) return next();
    bcrypt.hash(customer.password, SALT_WORK_FACTOR, function(err, hash) {
   
        if (err) return next(err);
        // override the cleartext password with the hashed one
        customer.password = hash;
        next();
    });
});

var member = Mongoose.model('member', MemberModel);
module.exports = {
    MemberModel: member
};