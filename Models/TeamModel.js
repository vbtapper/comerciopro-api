#!/usr/local/bin/node
'use strict';

const Mongoose              = require('mongoose');
const Schema                = Mongoose.Schema;

const TeamModel = new Schema({
    created: { type: Date, default: Date.now },
    status : {type : Boolean, default: true},
    description : { type: String, default: "none" },
    phone: { type: String, required: true},
    teamID: {type: String, required: true},
    accessKey: {type: String, required: true},
    location: {
        country: {type: String, required: true},
        city: {type: String, required: true},
        address: {type: String, required: true}
    },
    paymentType: [{
        name: {type: String, required: true},
        type: {type: String, required: true},
        cardType: {type: String, required: true},
        accessKeys: {
            test: {type: String, required: true},
            live: {type: String, required: true}
        }
    }],
    clientWeb: {
        name: {type: String, required: true},
        url : {type: String, required: true},
        repository: {type: String, required: true}
    }
});

var team = Mongoose.model('team', TeamModel);
module.exports = {
    TeamModel: team
};