#!/usr/local/bin/node
'use strict';

const Q                       = require('q');
const Hapi                    = require('hapi');
const AuthBearer              = require('hapi-auth-bearer-token');
const routes                  = require('./routes');
const config                  = require('./config');
const MemberModel             = require('./Models/MemberModel').MemberModel;
const TeamModel               = require('./Models/TeamModel').TeamModel;

var database                  = null;

const server = new Hapi.Server();
server.connection({ port: config.server.port });

function FindTeamBySessionToken(accessToken) {
    var deferred = Q.defer();
    MemberModel.findOne({'sessiontoken' : accessToken}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function FindTeamByAccessKey(accessKeyParam) {
    var deferred = Q.defer();
    TeamModel.findOne({'accessKey' : accessKeyParam}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

server.register(AuthBearer, (err) => {
    server.auth.strategy('SuperManager', 'bearer-access-token', {
        allowQueryToken: true,              
        allowMultipleHeaders: true,        
        accessTokenName: 'access_token', 
        validateFunc: function (token, callback) {          
            if (token != config.Admin.secretToken) {
                return callback(null, false);
            }
            else {
                return callback(null, true, config.Admin);
            }
        }
    });

    server.auth.strategy('Team', 'bearer-access-token', {
        allowQueryToken: true,              
        allowMultipleHeaders: true,        
        accessTokenName: 'access_token', 
        validateFunc: function (token, callback) {    
            FindTeamBySessionToken(token)
            .then(foundMember => {
                return callback(null, true, foundMember);
            })  
            .catch(err => {
                console.log('team failed access ' + err);
                return callback(null, false);
            })    
        }
    });

    server.auth.strategy('Client', 'bearer-access-token', {
        allowQueryToken: true,              
        allowMultipleHeaders: true,        
        accessTokenName: 'access_token', 
        validateFunc: function (token, callback) {    
            FindTeamByAccessKey(token)
            .then(foundTeam => {
                return callback(null, true, foundTeam);
            })  
            .catch(err => {
                console.log('client failed access ' + err);
                return callback(null, false);
            })    
        }
    });

    server.route(routes);
});

server.start(function (err) {
    if(!err) {
        database = require('./DataBase/MongoConnection');
        console.log('Server started at: ' + server.info.uri);
    }
    else {
        console.log(err)
    }
});