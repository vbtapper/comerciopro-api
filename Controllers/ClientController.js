#!/usr/local/bin/node
'use strict';

const randtoken     = require('rand-token').generator();
const Q             = require('q');
const Boom          = require('boom');
const bcrypt        = require('bcryptjs');
const async         = require("async");

const config        = require('../config');

//Models
const TeamModel     = require('../Models/TeamModel').TeamModel;
const MemberModel   = require('../Models/MemberModel').MemberModel;
const ProductModel  = require('../Models/ProductModel').ProductModel;
const ClientModel   = require('../Models/ClientModel').ClientModel;

function FindTeamByAccessKey(accessKeyParam) {
    var deferred = Q.defer();

    TeamModel.findOne({'accessKey' : accessKeyParam}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function AddNewClient(newClientObj) {
    var deferred = Q.defer();

    var newClient = new ClientModel(newClientObj)
    newClient.save(function(err, data) {
        if (err) {
            deferred.reject(err)
        }
        else {
            deferred.resolve(data)
        }
    });

    return deferred.promise;
}

module.exports = {
    JoinClient: function(request, reply) {
        FindTeamByAccessKey(request.auth.credentials.accessKey)
        .then(FoundTeam => {
            var newClientObj = {
                email: request.payload.email,
                firstname: request.payload.firstname,
                lastname: request.payload.lastname,
                teamID: FoundTeam.teamID,
                password: request.payload.password,
                addresses: request.payload.address
            }
            AddNewClient(newClientObj)
            .then(newClientResponse => {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'success'
                }
                return reply(infoResponse);
            })
            .catch(error => {
                console.log(error)
                return reply(Boom.internal('an internal error occured, please contact admin at support@comerciopro.com'))
            })
        })
        .catch(error => {
            console.log(error);
            return reply(Boom.notFound('Team Not Found'));
        })
    }
}