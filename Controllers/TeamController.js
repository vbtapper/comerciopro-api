#!/usr/local/bin/node
'use strict';

const randtoken     = require('rand-token').generator();
const Q             = require('q');
const Boom          = require('boom');
const bcrypt        = require('bcryptjs');
const async         = require("async");

const config        = require('../config');

//Models
const TeamModel     = require('../Models/TeamModel').TeamModel;
const MemberModel   = require('../Models/MemberModel').MemberModel;

function AddTeamHelper(teamObject) {
    var deferred = Q.defer();

    var newTeam = new TeamModel(teamObject)
    newTeam.save(function(err, data) {
        if (err) {
            deferred.reject(err)
        }
        else {
            deferred.resolve(data)
        }
    });

    return deferred.promise;
}

function AddMemberHelper(memberObject) {
    var deferred = Q.defer();
    var newMember = new MemberModel(memberObject)
    newMember.save(function(err, data) {
        if (err) {
            deferred.reject(err)
        }
        else {
            deferred.resolve(data)
        }
    });

    return deferred.promise;
}

function FindTeamByTeamID(teamIDParam) {
    var deferred = Q.defer();

    TeamModel.findOne({'teamID' : teamIDParam}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function FindMemberByEmail(emailParam) {
    var deferred = Q.defer();

    MemberModel.findOne({'email' : emailParam}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;   
}

function FindMemberBySessionToken(sessiontoken) {
    var deferred = Q.defer();

    MemberModel.findOne({'sessiontoken' : sessiontoken}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;   
}

function GetAllMembersByTeamID(teamIDParam) {
    var deferred = Q.defer();

    MemberModel.find({'teamID' : teamIDParam}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;   
}

function FindMemberByID(memberIDParam) {
    var deferred = Q.defer();

    MemberModel.findOne({'_id' : memberIDParam}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;     
}

function RemoveMember(memberIDParam) {
    var deferred = Q.defer();
    MemberModel.findOneAndRemove({'_id' : memberIDParam}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null);
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

module.exports = {

    AddTeam: function(request, reply) {
        var newTeamObject = {
            description : request.payload.description,
            phone: request.payload.phone,
            teamID: randtoken.generate(20, config.utils.sequence),
            accessKey: randtoken.generate(30, config.utils.sequence),
            location: {
                country: request.payload.country,
                city: request.payload.city,
                address: request.payload.address
            },
            paymentType: [{
                name: 'stripe',
                type: 'credit card',
                cardType: 'visa/mastercard',
                accessKeys: {
                    test: request.payload.paymentTestAccessKey,
                    live: request.payload.paymentLiveAccessKey
                }
            }],
            clientWeb: {
                name: request.payload.clientWebName,
                url : request.payload.clientWebUrl,
                repository: randtoken.generate(10, config.utils.sequence)
            }
        }
        AddTeamHelper(newTeamObject)
        .then(newTeamObj => {
            var tmpPassword = randtoken.generate(8, config.utils.sequence);
            console.log(tmpPassword)
            var newMemberObject = {
                email: request.payload.email,
                firstname: request.payload.firstname,
                lastname: request.payload.lastname,
                scope: ['OWNER'],
                teamID: newTeamObj.teamID,
                password: tmpPassword,
            }
            AddMemberHelper(newMemberObject)
            .then(newMemberObj => {
              
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'success'
                }
                return reply(infoResponse);
            })
            .catch(error => {
                console.log(error);
                return reply(Boom.badImplementation('An internal error occurred while creating the member'));
            })
        })
        .catch(error => {
            console.log(error);
            return reply(Boom.badImplementation('An internal error occurred while creating the team'));
        })
    },

    AddTeamMember: function(request, reply) {
        FindTeamByTeamID(request.payload.teamID)
        .then(FoundTeam => {

            var tmpPassword = randtoken.generate(8, config.utils.sequence);
            var newMemberObject = {
                email: request.payload.email,
                firstname: request.payload.firstname,
                lastname: request.payload.lastname,
                scope: request.payload.scope,
                teamID: request.payload.teamID,
                password: tmpPassword,
            }
            AddMemberHelper(newMemberObject)
            .then(newMemberObj => {
                console.log('New Member Temporary Password : ' + tmpPassword + ' TeamID : ' + request.payload.teamID)
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'success'
                }
                return reply(infoResponse)
            })
            .catch(error => {
                console.log(error)
                if(error['code'] === 11000) {
                    var infoResponse = {
                        statusCode: 500,
                        error: 'Internal Error',
                        message: 'email in use'
                    }
                    return reply(infoResponse);
                }
                else {
                    return reply(Boom.badImplementation('An internal error occurred while creating the member'));    
                }
            })
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('TeamID not found'));
        })
    },

    LoginMember: function(request, reply) {
        FindMemberByEmail(request.query.email)
        .then(FoundMember => {
            bcrypt.compare(request.query.password, FoundMember.password, function(err, isMatch) {
                if(err) {
                    return reply(Boom.badRequest('email or password does not match'))
                }
                else {
                    if(isMatch) {
                        
                        if(!FoundMember.active) {
                            var infoResponse = {
                                statusCode: 200,
                                error: null,
                                message: 'not active'
                            }
                            return reply(infoResponse)
                        }
                        else {
                            FindTeamByTeamID(FoundMember.teamID)
                            .then(FoundTeam => {
                                FoundMember.sessiontoken = randtoken.generate(20, config.utils.sequence);
                                FoundMember.save();
                               
                                var infoResponse = {
                                    statusCode: 200,
                                    error: null,
                                    message: 'access granted',
                                    sessiontoken: FoundMember.sessiontoken,
                                    firsttime: FoundMember.firsttimelogin,
                                    teamid: FoundMember.teamID,
                                    scope: FoundMember.scope.toString(),
                                    name: FoundMember.firstname + " " + FoundMember.lastname,
                                    repName: FoundTeam.clientWeb.repository
                                }

                                return reply(infoResponse)
                            })
                            .catch(error => {
                                console.log(error)
                                return reply(Boom.internal('an internal error occurred. check logs for details'))
                            })
                        }
                        
                    }
                    else {
                        return reply(Boom.badRequest('email or password does not match'))
                    }
                }
            })
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('Member not found'));  
        })
    },

    ChangePassword: function(request, reply) {
        FindMemberByEmail(request.payload.email)
        .then(FoundMember => {
            bcrypt.compare(request.payload.oldpassword, FoundMember.password, function(err, isMatch) {
                if(err) {
                    return reply(Boom.badRequest('old password does not match'))
                }
                else {
                    if(isMatch) {
                        
                        FoundMember.password = request.payload.newpassword;
                        FoundMember.save();

                        var infoResponse = {
                            statusCode: 200,
                            error: null,
                            message: 'password changed successfuly',
                        }

                        return reply(infoResponse)
                    }
                    else {
                        return reply(Boom.badRequest('old password does not match'))
                    }
                }
            })
        })  
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('Member not found'));  
        })  
    },

    ChangePasswordNoOld: function(request, reply) {
        FindMemberByEmail(request.payload.email)
        .then(FoundMember => {
         
            if(FoundMember.firsttimelogin) {
                FoundMember.firsttimelogin = false;
            }
            
            FoundMember.password = request.payload.newpassword;
            FoundMember.save();

            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'password changed successfuly',
            }

            return reply(infoResponse)
        })  
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('Member not found'));  
        })  
    },

    GetMembers: function(request, reply) {
        var result = [];
        
        function PushToResult(member) {
            if(member.scope.toString() !== "OWNER") {
                result.push({
                    "id" : member._id,
                    "name": member.firstname + " " + member.lastname,
                    "email" : member.email,
                    "active" : member.active,
                    "role" : member.scope.toString(),
                })
            }
        } 

        FindMemberBySessionToken(request.query.sessiontoken)
        .then(FoundMember => {
            GetAllMembersByTeamID(FoundMember.teamID)
            .then(members => {
                async.forEach(members, function (element, callback){ 
                    PushToResult(element); 
                    callback()
                }, function(err) {
                    var infoResponse = {
                        statusCode: 200,
                        error: null,
                        message: 'success',
                        data: result
                    }
                    return reply(infoResponse)
                });
            })
            .catch(err => {
                console.log(err);
                return reply(Boom.internal('An internal error occurred while fetching the members'))
            })
        })
        .catch(err => {
            console.log(err)
            return reply(Boom.notFound('owner not found'))
        })
    },

    ActivateMember: function(request, reply) {
        FindMemberByID(request.payload.memberID)
        .then(FoundMember => {
            FoundMember.active = true;
            FoundMember.save();

            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success'
            }
            return reply(infoResponse)
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.internal('An internal error occurred')); 
        })
    },

    DisactivateMember: function(request, reply) {
        FindMemberByID(request.payload.memberID)
        .then(FoundMember => {
            FoundMember.active = false;
            FoundMember.save();

            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success'
            }
            return reply(infoResponse)
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.internal('An internal error occurred')); 
        })
    },

    RemoveMemberRequest: function(request, reply) {
        RemoveMember(request.payload.memberID)
        .then(FoundMember => {
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success'
            }
            return reply(infoResponse)
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.internal('An internal error occurred')); 
        })   
    }
}