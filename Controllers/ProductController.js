#!/usr/local/bin/node
'use strict';

const randtoken     = require('rand-token').generator();
const Q             = require('q');
const Boom          = require('boom');
const bcrypt        = require('bcryptjs');
const async         = require("async");

const config        = require('../config');

//Models
const TeamModel     = require('../Models/TeamModel').TeamModel;
const MemberModel   = require('../Models/MemberModel').MemberModel;
const ProductModel  = require('../Models/ProductModel').ProductModel;

function AddProduct(newProductObject) {
    var deferred = Q.defer();

    var newProduct = new ProductModel(newProductObject)
    newProduct.save(function(err, data) {
        if (err) {
            deferred.reject(err)
        }
        else {
            deferred.resolve(data)
        }
    });

    return deferred.promise;
}


function FindMemberBySessionToken(sessiontoken) {
    var deferred = Q.defer();

    MemberModel.findOne({'sessiontoken' : sessiontoken}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;   
}

function GetAllProductsByTeamID(teamIDParam) {
    var deferred = Q.defer();

    ProductModel.find({'teamID' : teamIDParam}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;  
}

function GetProductDetailsByPublicID(productIDParam) {
    var deferred = Q.defer();

    ProductModel.findOne({'productPublicID' : productIDParam}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function RemoveProduct(productIDParam) {
    var deferred = Q.defer();
    ProductModel.findOneAndRemove({'productPublicID' : productIDParam }, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            data.remove();
            deferred.resolve(data);
        }
    });
    return deferred.promise;
}

function GetProductAllProductsToSellByTeamID(teamIDParam, limitParam) {
    var deferred = Q.defer();

    ProductModel.find({$and: [{ 'teamID' : teamIDParam }, {'data.status' : 'published'} ]}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    }).limit(limitParam);
    return deferred.promise;
}

function GetProductAllProductsToSellByAttributes(teamIDParam, limitParam, attributesArray) {
    var deferred = Q.defer();

    ProductModel.find({$and: [{ 'teamID' : teamIDParam }, {'data.status' : 'published'}, {'attributes.type': {$in: attributesArray}} ]}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    }).limit(limitParam);
    return deferred.promise;
}

module.exports = {
    AddNewProduct: function(request, reply) {
        var newProductObject = {
            productPublicID: randtoken.generate(20, config.utils.sequence),
            teamID: request.payload.teamID,
            general: {
                productName: request.payload.productName,
                metaTagTitle: request.payload.titleTag,
                metaTagDescription: request.payload.descriptionTag,
                metaTagKeywords: request.payload.keywordsTag,
                description: request.payload.description,   
                vendor: request.payload.manufacture,
            },
            data: {
                model: request.payload.model,
                category: request.payload.category,
                brand: request.payload.brand,
                location: request.payload.location,
                quantity: request.payload.quantity,
                status: "unpublished",
            },
            images: request.payload.images,
            costs: {
                sellingPrice: request.payload.price,
            },
            attributes: request.payload.attributes
        } 
        AddProduct(newProductObject)
        .then(NewProductObject => {
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
                publicID: NewProductObject.productPublicID
            }
            return reply(infoResponse)
        })  
        .catch(error => {
            return reply(Boom.internal('an internal error occurred, while adding the product. check logs for details'))
        })
    },

    GetProducts: function(request, reply) {
        var result = [];
        
        function PushToResult(product) {
            result.push({
                "id" : product.productPublicID,
                "name": product.general.productName,
                "model" : product.data.model,
                "price": product.costs.sellingPrice,
                "quantity": product.data.quantity,
                "status" : product.data.status,
                "image" : product.images[0].path,
            })
            
        } 

        FindMemberBySessionToken(request.query.sessiontoken)
        .then(FoundMember => {
            GetAllProductsByTeamID(FoundMember.teamID)
            .then(products => {
                async.forEach(products, function (element, callback){ 
                    PushToResult(element); 
                    callback()
                }, function(err) {
                    var infoResponse = {
                        statusCode: 200,
                        error: null,
                        message: 'success',
                        data: result
                    }
                    return reply(infoResponse)
                });
            })
            .catch(err => {
                console.log(err);
                return reply(Boom.internal('An internal error occurred while fetching the products'))
            })
        })
        .catch(err => {
            console.log(err)
            return reply(Boom.notFound('owner not found'))
        })
    },

    GetProductDetails: function(request, reply) {
        GetProductDetailsByPublicID(request.query.productID)
        .then(FoundProduct => {
            var statusBoolean = true;

            if(FoundProduct.data.status !== "unpublished") {
                statusBoolean = false;
            }

            var infoResponse = {
                product: {
                    created: FoundProduct.created,
                    general: {
                        productName: FoundProduct.general.productName,
                        metaTagTitle: FoundProduct.general.metaTagTitle,
                        metaTagDescription: FoundProduct.general.metaTagDescription,
                        metaTagKeywords: FoundProduct.general.metaTagKeywords,
                        description: FoundProduct.general.description,   
                        vendor: FoundProduct.general.vendor,
                    },
                    data: {
                        model: FoundProduct.data.model,
                        category: FoundProduct.data.category,
                        brand: FoundProduct.data.brand,
                        location: FoundProduct.data.location,
                        quantity: FoundProduct.data.quantity,
                        status: FoundProduct.data.status,
                        statusBoolean: statusBoolean
                    },
                    images: FoundProduct.images,
                    sellingPrice: FoundProduct.costs.sellingPrice,
                    attributes: FoundProduct.attributes,
                    publicID: FoundProduct.productPublicID
                },
                statusCode: 200,
                error: null,
                message: 'success'
            }
            return reply(infoResponse)
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('product not found'))
        })
    },

    PublishProduct: function(request, reply) {
        GetProductDetailsByPublicID(request.payload.productID)
        .then(FoundProduct => {
            FoundProduct.data.status = 'published';
            FoundProduct.save();

            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
                publicID: request.payload.productID
            }

            return reply(infoResponse);
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('product not found'))
        })
    },

    UnPublishProduct: function(request, reply) {
        GetProductDetailsByPublicID(request.payload.productID)
        .then(FoundProduct => {
            FoundProduct.data.status = 'unpublished';
            FoundProduct.save();

            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
                publicID: request.payload.productID
            }

            return reply(infoResponse);
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('product not found'))
        })
    },

    AddNewImage: function(request, reply) {
        GetProductDetailsByPublicID(request.payload.productID)
        .then(FoundProduct => {
            FoundProduct.images.push(request.payload.imageObj)
            FoundProduct.save();

            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
                images: FoundProduct.images
            }

            return reply(infoResponse);
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('product not found'))
        })
    },

    RemoveImage: function(request, reply) {
        GetProductDetailsByPublicID(request.payload.productID)
        .then(FoundProduct => {

            FoundProduct.images = FoundProduct.images.filter(obj => obj.name !== request.payload.imageID)
            FoundProduct.save();

            var arrangeP = 0;
            async.forEach(FoundProduct.images, function (element, callback){
                FoundProduct.images[arrangeP].position = arrangeP;
                FoundProduct.save();
                arrangeP = arrangeP + 1;

                callback();
            },function(err) {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'success',
                    images: FoundProduct.images
                }
                return reply(infoResponse);
            });
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('product not found'))
        })
    },

    ChangeImagePosition: function(request, reply) {
        GetProductDetailsByPublicID(request.payload.productID)
        .then(FoundProduct => {

            const elementObj1Position = FoundProduct.images[request.payload.position].position;
            const elementObj1Path = FoundProduct.images[request.payload.position].path;
            const elementObj1Name = FoundProduct.images[request.payload.position].name;

            const elementObj2Position = FoundProduct.images[request.payload.positionToGo].position;
            const elementObj2Path = FoundProduct.images[request.payload.positionToGo].path;
            const elementObj2Name = FoundProduct.images[request.payload.positionToGo].name;

            const elementObj1 = {
                path: elementObj1Path,
                name: elementObj1Name,
            }

            const elementObj2 = {
                path: elementObj2Path,
                name: elementObj2Name,
            }
            
            FoundProduct.images = FoundProduct.images.filter(obj => obj.name !== elementObj1Name)
            FoundProduct.images = FoundProduct.images.filter(obj => obj.name !== elementObj2Name)

            FoundProduct.images.push(elementObj1)
            FoundProduct.images.push(elementObj2)
            
            FoundProduct.save();

            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
                images: FoundProduct.images
            }
            return reply(infoResponse);
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('product not found'))
        })
    },

    RemoveAttribute: function(request, reply) {
        GetProductDetailsByPublicID(request.payload.productID)
        .then(FoundProduct => {

            FoundProduct.attributes = FoundProduct.attributes.filter(obj => obj.id !== request.payload.attributeID)
            FoundProduct.save();

            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
                attributes: FoundProduct.attributes
            }
            return reply(infoResponse);
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('product not found'))
        })
    },

    AddAttribute: function(request, reply) {
        GetProductDetailsByPublicID(request.payload.productID)
        .then(FoundProduct => {

            var newAttribute = {
                id: request.payload.id,
                type: request.payload.type,
                value: request.payload.value,
                key: request.payload.key,
                quantity: request.payload.quantity,
                additionalCost: request.payload.additionalCost,
                purchaseOption: request.payload.purchaseOption
            }
            
            FoundProduct.attributes.push(newAttribute)
            FoundProduct.save();

            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
                attributes: FoundProduct.attributes
            }
            return reply(infoResponse);
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('product not found'))
        })
    },

    EditProductData: function(request, reply) {
        GetProductDetailsByPublicID(request.payload.productID)
        .then(FoundProduct => {

            FoundProduct.data.location = request.payload.location;
            FoundProduct.costs.sellingPrice = request.payload.price;
            FoundProduct.data.quantity = request.payload.quantity;
            FoundProduct.general.vendor = request.payload.vendor;
            
            FoundProduct.save();

            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
                publicID: FoundProduct.productPublicID
            }
            return reply(infoResponse);
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('product not found'))
        })
    },

    EditProductGeneral: function(request, reply) {
        GetProductDetailsByPublicID(request.payload.productID)
        .then(FoundProduct => {

            FoundProduct.general.productName = request.payload.productName;
            FoundProduct.general.metaTagTitle = request.payload.titleTag;
            FoundProduct.general.metaTagDescription = request.payload.descriptionTag;
            FoundProduct.general.metaTagKeywords = request.payload.keywordsTag
            FoundProduct.general.description = request.payload.description;
            
            FoundProduct.save();

            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
                publicID: FoundProduct.productPublicID
            }
            return reply(infoResponse);
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('product not found'))
        })
    },

    RemoveProduct: function(request, reply) {
        RemoveProduct(request.payload.productID)
        .then(RemovedResponse => {
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
                images: RemovedResponse.images
            }
            return reply(infoResponse);
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('product not found'))
        })
    },

    //Clients
    GetProductsToSell: function(request, reply) {
        GetProductAllProductsToSellByTeamID(request.auth.credentials.teamID, request.query.limit)
        .then(FoundProducts => {
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
                products: FoundProducts,
                numberOfRecords: FoundProducts.length
            }
            return reply(infoResponse)
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.internal('an internal error occured. please contact admin at supportapi@comerciopro.com'))
        })
    },

     GetProductsToSellByAttributes: function(request, reply) {
        GetProductAllProductsToSellByAttributes(request.auth.credentials.teamID, request.payload.limit, request.payload.attributes)
        .then(FoundProducts => {
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
                products: FoundProducts
            }
            return reply(infoResponse)
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.internal('an internal error occured. please contact admin at supportapi@comerciopro.com'))
        })
    },

    GetClientProductDetails: function(request, reply) {
        GetProductDetailsByPublicID(request.query.productID)
        .then(FoundProduct => {

            var infoResponse = {
                product: {
                    general: {
                        productName: FoundProduct.general.productName,
                        metaTagTitle: FoundProduct.general.metaTagTitle,
                        metaTagDescription: FoundProduct.general.metaTagDescription,
                        metaTagKeywords: FoundProduct.general.metaTagKeywords,
                        description: FoundProduct.general.description,   
                        vendor: FoundProduct.general.vendor,
                    },
                    data: {
                        model: FoundProduct.data.model,
                        category: FoundProduct.data.category,
                        location: FoundProduct.data.location,
                        quantity: FoundProduct.data.quantity,
                        status: FoundProduct.data.status,
                    },
                    images: FoundProduct.images,
                    sellingPrice: FoundProduct.costs.sellingPrice,
                    attributes: FoundProduct.attributes,
                    publicID: FoundProduct.productPublicID
                },
                statusCode: 200,
                error: null,
                message: 'success'
            }
            return reply(infoResponse)
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('product not found'))
        })
    },

    GetAttributeByKey: function(request, reply) {
        GetProductDetailsByPublicID(request.query.productID)
        .then(FoundProduct => {

            var resultArray = [];
            var attributeKey = FoundProduct.attributes.filter(obj => obj.id === request.query.attributeID)[0].key;

            async.forEach(FoundProduct.attributes, function (element, callback){
                if(element.key === attributeKey) {
                    resultArray.push(element);
                }    

                callback();
            },function(err) {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'success',
                    attributes: resultArray
                }
                return reply(infoResponse);
            });
        })
        .catch(error => {
            console.log(error)
            return reply(Boom.notFound('product not found'))       
        })
    }
}