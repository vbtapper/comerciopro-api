#!/usr/local/bin/node
'use strict';

const TeamController                         = require('./Controllers/TeamController');
const ProductController                      = require('./Controllers/ProductController');
const ClientController                       = require('./Controllers/ClientController');

var Joi                                      = require('joi');

module.exports = [
    {
        method: 'POST',
        path: '/addteam',
        config: {
            auth: {
                strategy: 'SuperManager',
                scope: 'Admin'
            },
            validate: {
                payload : {
                    description: Joi.string().required(),
                    phone: Joi.string().required(),
                    country: Joi.string().required(),
                    city: Joi.string().required(),
                    address: Joi.string().required(),
                    paymentTestAccessKey: Joi.string().required(),
                    paymentLiveAccessKey: Joi.string().required(),
                    clientWebName: Joi.string().required(),
                    clientWebUrl: Joi.string().required(),
                    email: Joi.string().required(),
                    firstname: Joi.string().required(),
                    lastname: Joi.string().required()
                }
            },
            handler: TeamController.AddTeam
        }
    },

    {
        method: 'POST',
        path: '/addmember',
        config: {
            auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                payload : {
                    email: Joi.string().required(),
                    firstname: Joi.string().required(),
                    lastname: Joi.string().required(),
                    teamID: Joi.string().required(),
                    scope: Joi.string().valid('FINANCES', 'OPERATOR')
                }
            },
        },
        handler: TeamController.AddTeamMember
    },

    {
        method: 'GET',
        path: '/memberlogin',
        config: {
            auth: {
                strategy: 'SuperManager',
                scope: 'Admin'
            },
            validate: {
                query : {
                    email: Joi.string().required(),
                    password: Joi.string().required()
                }
            }
        },
        handler: TeamController.LoginMember
    },

    {
        method: 'PATCH',
        path: '/changepassword',
        config: {
            auth: {
                strategy: 'SuperManager',
                scope: 'Admin'
            },
            validate: {
                payload: {
                    email: Joi.string().required(),
                    oldpassword: Joi.string().required(),
                    newpassword: Joi.string().required(),
                }
            }
        },
        handler: TeamController.ChangePassword
    },

    {
        method: 'PATCH',
        path: '/chpswd',
        config: {
            auth: {
                strategy: 'SuperManager',
                scope: 'Admin'
            },
            validate: {
                payload: {
                    email: Joi.string().required(),
                    newpassword: Joi.string().required(),
                }
            }
        },
        handler: TeamController.ChangePasswordNoOld
    },

    {
        method: 'GET',
        path: '/getmembers',
        config: {
            auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                query : {
                    sessiontoken: Joi.string().required()
                }
            }
        },
        handler: TeamController.GetMembers
    },

    {
        method: 'PATCH',
        path: '/actmember',
        config: {
            auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                payload : {
                    memberID: Joi.string().required()
                }
            },
            handler: TeamController.ActivateMember
        }
    },

    {
        method: 'PATCH',
        path: '/disactmember',
        config: {
            auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                payload : {
                    memberID: Joi.string().required()
                }
            },
            handler: TeamController.DisactivateMember
        }
    },

    {
        method: 'PATCH',
        path: '/removemember',
        config: {
            auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                payload : {
                    memberID: Joi.string().required()
                }
            },
            handler: TeamController.RemoveMemberRequest
        }
    },

    {
        method: 'POST',
        path: '/addproduct',
        config: {
            auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                payload: {
                    teamID: Joi.string().required(),
                    productName: Joi.string().required(),
                    titleTag: Joi.string().required(),
                    descriptionTag: Joi.string().required(),
                    keywordsTag: Joi.string(),
                    brand: Joi.string().required(),
                    description: Joi.string().required(),
                    model: Joi.string().required(),
                    category: Joi.string().required(),
                    location: Joi.string().required(),
                    quantity: Joi.number().required(),
                    price: Joi.string().required(),
                    manufacture: Joi.string().required(),
                    images: Joi.array(),
                    attributes: Joi.array()
                }
            },
            handler: ProductController.AddNewProduct
        }
    },

    {
        method: 'GET',
        path: '/getproducts',
        config: {
            auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                query : {
                    sessiontoken: Joi.string().required()
                }
            }
        },
        handler: ProductController.GetProducts
    },

    {
        method: 'GET',
        path: '/getproductdetail',
        config: {
            auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                query : {
                    productID: Joi.string().required()
                }
            },
            handler: ProductController.GetProductDetails
        }
    },

    {
        method: 'PATCH',
        path: '/publishproduct',
        config: {
            auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                payload : {
                    productID: Joi.string().required()
                }
            },
            handler: ProductController.PublishProduct
        }
    },

    {
        method: 'PATCH',
        path: '/unpublishproduct',
        config: {
            auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                payload : {
                    productID: Joi.string().required()
                }
            },
            handler: ProductController.UnPublishProduct
        }
    },

    {
        method: 'PATCH',
        path: '/addimageproduct',
        config: {
            auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                payload : {
                    productID: Joi.string().required(),
                    imageObj: Joi.object()
                }
            },
            handler: ProductController.AddNewImage
        }
    },

    {
        method: 'PATCH',
        path: '/removeimageproduct',
        config: {
            auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                payload : {
                    productID: Joi.string().required(),
                    imageID: Joi.string().required()
                }
            },
            handler: ProductController.RemoveImage
        }
    },

    {
        method: 'PATCH',
        path: '/changepositionimageproduct',
        config: {
            auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                payload : {
                    productID: Joi.string().required(),
                    position: Joi.number().required(),
                    positionToGo: Joi.number().required()
                }
            },
            handler: ProductController.ChangeImagePosition
        }
    },

    {
        method: 'PATCH',
        path: '/removeattributeproduct',
        config: {
            auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                payload : {
                    productID: Joi.string().required(),
                    attributeID: Joi.string().required()
                }
            },
            handler: ProductController.RemoveAttribute
        }
    },

    {
        method: 'PATCH',
        path: '/addattributeproduct',
        config: {
             auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                payload: {
                    id: Joi.string().required(),
                    type: Joi.string().required(),
                    value: Joi.string().required(),
                    key: Joi.string(),
                    quantity: Joi.number().required(),
                    additionalCost: Joi.string(),
                    purchaseOption: Joi.boolean().required(),
                    productID: Joi.string().required()
                }
            },
            handler: ProductController.AddAttribute
        }
    },
 
    {
        method: 'PATCH',
        path: '/editproductdata',
        config: {
             auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                payload: {
                    location: Joi.string().required(),
                    price: Joi.string().required(),
                    quantity: Joi.number().required(),
                    vendor: Joi.string().required(),
                    productID: Joi.string().required()
                }
            },
            handler: ProductController.EditProductData
        }
    },

    {
        method: 'PATCH',
        path: '/editproductgeneral',
        config: {
             auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                payload: {
                    productName: Joi.string().required(),
                    titleTag: Joi.string().required(),
                    descriptionTag: Joi.string().required(),
                    description: Joi.string().required(),
                    keywordsTag: Joi.string().required(),
                    productID: Joi.string().required()
                }
            },
            handler: ProductController.EditProductGeneral
        }
    },
    
    {
        method: 'PATCH',
        path: '/removeproduct',
        config: {
            auth: {
                strategy: 'Team',
                scope: 'OWNER'
            },
            validate: {
                payload : {
                    productID: Joi.string().required()
                }
            },
            handler: ProductController.RemoveProduct
        }
    },

    //CLIENT ROUTES
    {
        method: 'GET',
        path: '/client/getproductssell',
        config: {
            auth: {
                strategy: 'Client',
            },
            validate: {
                query: {
                    limit: Joi.number().required()
                }
            },
            handler: ProductController.GetProductsToSell
        }
    },

    {
        method: 'POST',
        path: '/client/getproductssellbyattributes',
        config: {
            auth: {
                strategy: 'Client',
            },
            validate: {
                payload: {
                    limit: Joi.number().required(),
                    attributes: Joi.array()
                }
            },
            handler: ProductController.GetProductsToSellByAttributes
        }
    },

    {
        method: 'GET',
        path: '/client/getproductdetails',
        config: {
            auth: {
                strategy: 'Client',
            },
            validate: {
                query: {
                    productID: Joi.string().required()
                }
            },
            handler: ProductController.GetClientProductDetails
        }
    },

    {
        method: 'GET',
        path: '/client/getattributesbykey',
        config: {
            auth: {
                strategy: 'Client',
            },
            validate: {
                query: {
                    attributeID: Joi.string().required(),
                    productID: Joi.string().required()
                }
            },
            handler: ProductController.GetAttributeByKey
        }
    },

    {
        method: 'POST',
        path: '/client/addclient',
        config: {
            auth: {
                strategy: 'Client',
            },
            validate: {
                payload: {
                    email: Joi.string().email().required(),
                    firstname: Joi.string().required(),
                    lastname: Joi.string().required(),
                    password: Joi.string().required(),
                    address: Joi.array()
                }
            },
            handler: ClientController.JoinClient
        }
    }
]